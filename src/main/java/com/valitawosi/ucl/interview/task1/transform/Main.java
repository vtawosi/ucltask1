package com.valitawosi.ucl.interview.task1.transform;

import com.valitawosi.ucl.interview.task1.log.CustomLogger;

import java.io.IOException;

/**
 * @author vali.tawosi@gmail.com
 *         Date: 7/6/2018
 */

public class Main {

    public static void main(String[] args) {
        try {
            CustomLogger.setup();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Problems with creating the log files");
        }
        MainForm mainForm = new MainForm();
        mainForm.setVisible(true);
    }
}
