package com.valitawosi.ucl.interview.task1.transform;

import com.valitawosi.ucl.interview.task1.srcmltransformer.Language;
import com.valitawosi.ucl.interview.task1.srcmltransformer.SrcMlFileReader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileAttribute;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author vali.tawosi@gmail.com
 *         Date: 7/6/2018
 */

public class Executor {

    private String line;
    private OutputStream stdin = null;
    private InputStream stderr = null;
    private InputStream stdout = null;
    private String sourceDirectory;
    private String destDirectory;
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public Executor(String sourceDirectory, String destDirectory) {
        this.sourceDirectory = sourceDirectory;
        this.destDirectory = destDirectory;
    }

    public boolean execute() {
        try {
            LOGGER.setLevel(Level.ALL);
            LOGGER.info("Start!");

            //Preparing the directories
            Path tempDirectory = new File(destDirectory + File.separator + "TempDir").toPath();
            if (!Files.exists(tempDirectory))
                tempDirectory = Files.createDirectory(tempDirectory);

            Path processDirectory = new File(destDirectory + File.separator + "ProcessedDir").toPath();
            if (!Files.exists(processDirectory))
                processDirectory = Files.createDirectory(processDirectory);

            Path tempFile = new File(tempDirectory + File.separator + "tempIntermediate.xml").toPath();
            if (Files.exists(tempFile))
                Files.delete(tempFile);
            tempFile = Files.createFile(tempFile);

            //Use srcML to build xml file out of input Java file.
            toXml(sourceDirectory, tempFile.toString());
            //Doing the transformation by editing the xml file.
            String transformedFile = doTransform(tempFile, processDirectory);
            //Using scrML again to build Java file out of edited xml file.
            backToJava(transformedFile, destDirectory, sourceDirectory);

            LOGGER.info("Executed Successfully!");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.throwing("com.valitawosi.ucl.interview.task1.transform.Executor", "execute", e);
            return false;
        }
    }

    private String doTransform(Path filePath, Path processDirectory) throws Exception {
        SrcMlFileReader reader = new SrcMlFileReader(new File(filePath.toString()), Language.JAVA);
        return reader.transform(filePath.toString(), processDirectory.toString());
    }

    private void toXml(String sourceDirectory, String destDirectory) throws Exception {
        String[] commandParts = new String[]{"srcml"
                , sourceDirectory
                , "-o"
                , destDirectory
        };
        Process process = Runtime.getRuntime().exec(commandParts);
        stdin = process.getOutputStream();
        stderr = process.getErrorStream();
        stdout = process.getInputStream();

        BufferedReader brCleanUp =
                new BufferedReader(new InputStreamReader(stdout));
        while ((line = brCleanUp.readLine()) != null) {
            System.out.println("[Stdout] " + line);
        }
        brCleanUp.close();


        String error = "";
        boolean executedWithError = false;
        brCleanUp =
                new BufferedReader(new InputStreamReader(stderr));
        while ((line = brCleanUp.readLine()) != null) {
            executedWithError = true;
            error += line;
        }
        brCleanUp.close();
        if (executedWithError) throw new Exception(error);
    }

    private void backToJava(String xmlDirectory, String destDirectory, String sourceDirectory) throws Exception {
        String sourceFileName = new File(sourceDirectory).getName();

        Path resultDirectory = new File(destDirectory + File.separator + "ResultDir").toPath();
        if (!Files.exists(resultDirectory))
            resultDirectory = Files.createDirectory(resultDirectory);

        String resultJavaFile = resultDirectory + File.separator + sourceFileName;

        Path javaFile = new File(resultJavaFile).toPath();
        if (Files.exists(javaFile))
            Files.delete(javaFile);

        String[] commandParts = new String[]{"srcml"
                , xmlDirectory
                , "-o"
                , resultDirectory + File.separator + sourceFileName
        };
        Process process = Runtime.getRuntime().exec(commandParts);
        stdin = process.getOutputStream();
        stderr = process.getErrorStream();
        stdout = process.getInputStream();

        BufferedReader brCleanUp =
                new BufferedReader(new InputStreamReader(stdout));
        while ((line = brCleanUp.readLine()) != null) {
            System.out.println("[Stdout] " + line);
        }
        brCleanUp.close();


        String error = "";
        boolean executedWithError = false;
        brCleanUp =
                new BufferedReader(new InputStreamReader(stderr));
        while ((line = brCleanUp.readLine()) != null) {
            executedWithError = true;
            error += line;
        }
        brCleanUp.close();
        if (executedWithError) throw new Exception(error);
    }
}
