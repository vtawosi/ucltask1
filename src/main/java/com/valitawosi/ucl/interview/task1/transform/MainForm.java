package com.valitawosi.ucl.interview.task1.transform;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author vali.tawosi@gmail.com
 *         Date: 7/6/2018
 */

public class MainForm extends JFrame {

    private JButton sourceButton = new JButton();
    private JButton destButton = new JButton();
    private JButton executeButton = new JButton();
    private JLabel sourceDirAddressLabel = new JLabel();
    private JLabel destDirAddressLabel = new JLabel();
    private JLabel executeMessage = new JLabel();
    private JLabel descriptionInput = new JLabel();
    private JLabel descriptionOutput = new JLabel();
    private JLabel descriptionIntention = new JLabel();
    private JLabel extraDescription = new JLabel();
    private JFileChooser fileChooser = new JFileChooser();
    private String sourcePath;
    private String destPath;

    public MainForm() throws HeadlessException {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 400);
        this.setResizable(false);
        this.setLayout(null);
        this.setTitle("srcML Transformer Tool (for Special Purpose!)");

        fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new java.io.File("."));
        fileChooser.setDialogTitle("Source Directory");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fileChooser.setAcceptAllFileFilterUsed(false);

        sourceDirAddressLabel.setBounds(300, 20, 400, 30);
        this.add(sourceDirAddressLabel);

        descriptionInput.setBounds(100, 240, 700, 30);
        this.add(descriptionInput);
        descriptionInput.setText("Input: A Java File.");

        descriptionOutput.setBounds(100, 260, 700, 30);
        this.add(descriptionOutput);
        descriptionOutput.setText("Output: The same Java file fed in the input, injected " +
                "standard output log under every assignment in the code!");

        extraDescription.setBounds(100, 280, 700, 30);
        this.add(extraDescription);
        extraDescription.setText("To use this application you need to download srcML and set it's location in your OS's $PATH variable.");

        descriptionIntention.setBounds(130, 200, 700, 30);
        this.add(descriptionIntention);
        descriptionIntention.setText("This application is made in the fulfilment of a programming task for an Interview.");

        sourceButton.setText("Choose the Source File");
        sourceButton.setBounds(20, 20, 250, 30);
        this.add(sourceButton);
        sourceButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sourceButtonClicked(e);
            }
        });

        destDirAddressLabel.setBounds(300, 60, 400, 30);
        this.add(destDirAddressLabel);

        destButton.setText("Choose the Destination Directory");
        destButton.setBounds(20, 60, 250, 30);
        this.add(destButton);
        destButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                destButtonClicked(e);
            }
        });

        executeMessage.setBounds(300, 100, 400, 30);
        this.add(executeMessage);

        executeButton.setText("Go!");
        executeButton.setBounds(20, 100, 250, 30);
        this.add(executeButton);
        executeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                execButtonClicked(e);
            }
        });

    }


    private void sourceButtonClicked(ActionEvent e) {
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.FILES_ONLY) {
            File selectedFile = fileChooser.getSelectedFile();
            sourceDirAddressLabel.setText(sourcePath = selectedFile.getAbsolutePath());
        }
    }

    private void destButtonClicked(ActionEvent e) {
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            destDirAddressLabel.setText(destPath = selectedFile.getAbsolutePath());
        }
    }

    private void execButtonClicked(ActionEvent e) {
        if(sourcePath==null || destPath==null) {
            executeMessage.setText("Source and/or Destination Unknown!");
            return;
        }
        Executor executor = new Executor(sourcePath, destPath);
        boolean execute = executor.execute();
        executeMessage.setForeground(execute ? Color.BLUE : Color.RED);
        executeMessage.setText(execute ? "Successful!" : "There was at least one error! please see the log file :( ");
    }
}
