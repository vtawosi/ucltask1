package com.valitawosi.ucl.interview.task1.srcmltransformer;

import nu.xom.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static java.util.logging.Logger.GLOBAL_LOGGER_NAME;
import static java.util.logging.Logger.getLogger;
import java.util.logging.Logger;

/**
 * @author vali.tawosi@gmail.com
 *         Date: 7/1/2018
 */
public class SrcMlFileReader {

    public enum BLOCK_TYPE {BLOCK, PSEUDO}

    public enum BLOCK_HOST {CLASS, FUNCTION, BLOCK}

    private final String BLOCK_TAG = "block";
    private final String FUNCTION_TAG = "function";
    private final String CLASS_TAG = "class";
    private final String BLOCK_TYPE_ATTRIBUTE_NAME = "type";
    private final String PSEUDO_BLOCK_TYPE_NAME = "pseudo";
    private final String NAME_TAG = "name";
    private final String LANGUAGE_TAG = "language";
    private final String SRCML_TAG = "srcML";
    private final String URI_TAG = "xmlns";
    private final String FILENAME_ATTRIBUTE_NAME = "filename";


    private final List<String> ASSIGNMENT_OPERATORS = Arrays.asList(
            "=",
            "+=",
            "-=",
            "*=",
            "/=",
            "%=",
            "&=",
            "|=",
            "^=",
            ">>=",
            "<<=",
            ">>>="
    );
    private final List<String> ASSIGNMENT_TAGS = Arrays.asList(
            "operator",
            "init"
    );
    private final List<String> STATEMENT_TAGS = Arrays.asList(
            "decl_stmt",
            "expr_stmt"
    );
    private final List<String> BLOCK_HOST_TAGS = Arrays.asList(
            CLASS_TAG,
            FUNCTION_TAG,
            BLOCK_TAG
    );

    private class BlockInfo {
        public Element blockNode;
        public BLOCK_TYPE type;
        public BLOCK_HOST host;
    }

    private Document document;
    private String fileName;

    private final static Logger LOGGER = getLogger(GLOBAL_LOGGER_NAME);

    public SrcMlFileReader(File file, Language language) throws Exception {
        try {
            if (file == null || language == null)
                throw new Exception("NULL Argument!");
            System.out.println(file.getName());
            document = new Builder().build(file);
            Element rootElement = document.getRootElement();
            if (rootElement.getNamespaceURI().indexOf(SRCML_TAG) < 0)
                throw new Exception("This is not a SrcML generated file!");
            if (!language.getValue().equals(rootElement.getAttributeValue(LANGUAGE_TAG)))
                throw new Exception("This file is not generated from a " + language.getValue() + " file!" +
                        "\nGet its language specific Transformer (" +
                        rootElement.getAttributeValue(LANGUAGE_TAG) + " Transformer)");
            Attribute fileNameAttribute = rootElement.getAttribute(FILENAME_ATTRIBUTE_NAME);
            if (fileNameAttribute != null) {
                fileName = fileNameAttribute.getValue();
                fileName = fileName.substring(0, fileName.lastIndexOf('.'));
            }
        } catch (IOException ioe) {
            throw new Exception("Input File is not readable!", ioe);
        } catch (ParsingException e) {
            throw new Exception("Input File doesn't have a valid structure!", e);
        }
    }

    public String transform(String destFileName, String processedDirectory) throws Exception {
        if (document == null)
            throw new Exception("ّFile has not been read yet!");
        Element rootElement = document.getRootElement();
        checkForAssignment(rootElement);
        String destination = "";

        try {
            File result = new File(destFileName);
            String fileName = result.getName();
            fileName = "Transformed-" + fileName;
            destination = processedDirectory + File.separator + fileName;
            Path destFile = new File(destination).toPath();
            if (Files.exists(destFile))
                Files.delete(destFile);
            FileWriter writer = new FileWriter(destination);
            writer.write(document.toXML());
            writer.close();
        } catch (IOException iox) {
            LOGGER.info(iox.getMessage());
        }
        return destination;
    }

    private void checkForAssignment(Element e) {
        if (ASSIGNMENT_TAGS.contains(e.getLocalName().trim()))
            if (ASSIGNMENT_OPERATORS.contains(e.getChild(0).getValue().trim()))
                processAssignmentNode(e);
        Elements children = e.getChildElements();
        for (int i = 0; i < children.size(); i++) {
            checkForAssignment(children.get(i));
        }
    }

    private Element getImmediateOlderSibling(Element e) {
        ParentNode parent = e.getParent();
        if (parent == null) return null;
        if (parent.indexOf(e) == 0) return null;
        for (int i = parent.indexOf(e) - 1; i < parent.getChildCount(); i--) {
            if (parent.getChild(i) instanceof Element)
                return (Element) parent.getChild(i);
        }
        return null;
    }

    private void processAssignmentNode(Element e) {
        Element rightHandParameter = getImmediateOlderSibling(e);
        if (rightHandParameter != null && NAME_TAG.equals(rightHandParameter.getLocalName())) {
            String variableName = rightHandParameter.getValue();
            Element statement = getTheParentExpressionNode(e);
            if (statement != null) {
                BlockInfo blockInfo = getBlockInfo(statement);
                blockInfo.host = getBlockHost(blockInfo.blockNode);
                if (BLOCK_HOST.CLASS.equals(blockInfo.host))
                    //Do not inject print in class body! it will cause compile Error!
                    return;
                else {
                    if (BLOCK_TYPE.PSEUDO.equals(blockInfo.type)) {
                        Attribute attribute = blockInfo.blockNode.getAttribute(BLOCK_TYPE_ATTRIBUTE_NAME);
                        blockInfo.blockNode.removeAttribute(attribute);
                        Element newBlock = addCurlyBraces(blockInfo.blockNode);
                        ParentNode parent = blockInfo.blockNode.getParent();
                        int index = parent.indexOf(blockInfo.blockNode);
                        parent.removeChild(index);
                        parent.insertChild(newBlock, index);
                        removeAttribute(newBlock, URI_TAG);
                        statement = (Element) newBlock.getChild(1);
                    }
                    Element newPrintStatement = preparePrintStatement(variableName);
                    if (newPrintStatement != null) {
                        int index = statement.getParent().indexOf(statement);
                        statement.getParent().insertChild(newPrintStatement, index + 1);
                        removeAttribute(newPrintStatement, URI_TAG);
                    }
                    LOGGER.info(statement.getValue()+" is Handled!");
                }
            }
        }
    }

    private void removeAttribute(Element element, String attribute) {
        Attribute elementAttribute = element.getAttribute(attribute);
        if (elementAttribute != null)
            element.removeAttribute(elementAttribute);
    }

    private BLOCK_HOST getBlockHost(Element block) {
        ParentNode parent = block.getParent();
        if (parent == null) return null;
        Element blockHost = (Element) parent;
        BLOCK_HOST hostType;
        if (BLOCK_HOST_TAGS.contains(blockHost.getLocalName())) {
            switch (blockHost.getLocalName()) {
                case BLOCK_TAG:
                    hostType = BLOCK_HOST.BLOCK;
                    break;
                case FUNCTION_TAG:
                    hostType = BLOCK_HOST.FUNCTION;
                    break;
                case CLASS_TAG:
                    hostType = BLOCK_HOST.CLASS;
                    break;
                default:
                    hostType = BLOCK_HOST.CLASS;
            }
            return hostType;
        } else
            return getBlockHost(blockHost);
    }

    private BlockInfo getBlockInfo(Element statement) {
        ParentNode parent = statement.getParent();
        if (parent == null) return null;
        BlockInfo blockInfo = new BlockInfo();
        Element block = (Element) parent;
        if (BLOCK_TAG.equals(block.getLocalName())) {
            if (block.getAttribute(BLOCK_TYPE_ATTRIBUTE_NAME) != null && PSEUDO_BLOCK_TYPE_NAME.equals(block.getAttribute(BLOCK_TYPE_ATTRIBUTE_NAME).getValue()))
                blockInfo.type = BLOCK_TYPE.PSEUDO;
            else
                blockInfo.type = BLOCK_TYPE.BLOCK;
            blockInfo.blockNode = block;
            return blockInfo;
        } else
            return getBlockInfo(block);
    }

    private Element getTheParentExpressionNode(Element assignmentOperation) {
        ParentNode parent = assignmentOperation.getParent();
        if (parent == null) return null;
        if (!(parent instanceof Element))
            //it may be the root node! (when the assignment is an initialization inside a for, it is!)
            return null;
        Element expression = (Element) parent;
        if (STATEMENT_TAGS.contains(expression.getLocalName()))
            return expression;
        else
            return getTheParentExpressionNode(expression);
    }

    private Element preparePrintStatement(String parameterName) {
        StringBuilder printStatement = new StringBuilder();
        printStatement.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        printStatement.append("<expr_stmt>\n<expr><call><name><name>System</name><operator>.</operator><name>out</name><operator>.</operator><name>println</name></name><argument_list>(<argument><expr><literal type=\"string\">\"");
        printStatement.append(parameterName);
        printStatement.append(" = \"</literal> <operator>+</operator> <name>");
        printStatement.append(parameterName);
        printStatement.append("</name></expr></argument>)</argument_list></call></expr>;</expr_stmt>");
        return buildNodeOutOfXml(printStatement.toString());
    }

    private Element addCurlyBraces(Element blockNode) {
        StringBuilder newValue = new StringBuilder();
        newValue.append(blockNode.toXML());
        newValue.insert(newValue.indexOf(">") + 1, "{");
        newValue.insert(newValue.lastIndexOf("<"), "}");
        newValue.insert(0, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        return buildNodeOutOfXml(newValue.toString());
    }

    private Element buildNodeOutOfXml(String xml) {
        Builder parser = new Builder();
        Document tempDocument;
        Element newElement = null;
        StringReader reader = new StringReader(xml);
        try {
            tempDocument = parser.build(reader);
            newElement = (Element) tempDocument.getRootElement().copy();
        } catch (IOException | ParsingException e) {
            LOGGER.info(e.getMessage());
        }
        return newElement;
    }
}
