package com.valitawosi.ucl.interview.task1.srcmltransformer;

/**
 * @author vali.tawosi@gmail.com
 *         Date: 7/5/2018
 */

public enum Language {
    JAVA("Java"),
    C("C"),
    CPP("C++"),
    C_SHARP("C#");

    private final String value;

    Language(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
